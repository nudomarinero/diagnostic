
from __future__ import print_function
from setuptools import setup

import diagnostic

setup(
    name='diagnostic',
    version=diagnostic.__version__,
    url='https://bitbucket.org/nudomarinero/diagnostic',
    license='MIT',
    author='Jose Sabater',
    tests_require=['pytest'],
    install_requires=['numpy'],
    author_email='jsm@iaa.es',
    description='Diagnostic methods to classify Active Galactic Nuclei',
    packages=['diagnostic'],
    )